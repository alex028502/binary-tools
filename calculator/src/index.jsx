const React = require('react');
const ReactDOM = require('react-dom');
const App = require('./App.jsx');

const reduxDevToolsExt = require('./redux-dev-tools-ext');

const createStore = require('redux').createStore;
const reducer = require('./reducer');

const store = createStore(
  reducer,
  reduxDevToolsExt(window),
);

ReactDOM.render(<App store={store} />, document.getElementById('root'));

