#! /usr/bin/env bash

set -e


set -x
answer=$(node to-decimal.js 1i3.i)
[[ "$answer" == "27.5" ]]
set +x

echo yeah - command line parser tool seems to work
