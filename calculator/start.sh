#! /usr/bin/env bash

set -e

WDS_PREFIX=calc/ node_modules/.bin/webpack-dev-server --port 3000 --content-base public $@
