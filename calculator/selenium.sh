#! /usr/bin/env bash

set -e



set -m # start child processes in their own group
# NOTE I think I have a better way to do this now - the problem is
# that now this script is in a _different_ group the processes it starts
# so if you ctrl+c _this_ script, you are dependent on the cleanup script
# working properly - in newer projects I am working on just tracking down
# every child process from the test wrapper as a way of simulating ctrl+c

source cleanup.sh

label="Compiled successfully"
output=.server_output.txt
DEVTOOL=source-map INSTRUMENT=1 npm start -- $SERVER_ARGS 2>&1 | tee $output &
waited=""
maxwait=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
echo max wait: $maxwait
while ! grep "$label" $output
do
  waited="X$waited"
  echo $waited
  [[ "$waited" != $maxwait ]]
  sleep 1
done

# just run pytest alone if you already have the server running
if [[ "$SERVER_ADDRESS" == "" ]]
then
  opts="--server-address http://localhost:3000/"
else
  opts="--server-address http://$SERVER_ADDRESS:3000/"
  # these are two slightly different concepts that I don't have good
  # names for so giving them numbers rather than risking bad names
  opts="$opts --proxy-host-1=$SERVER_ADDRESS"
  opts="$opts --proxy-host-2=$SERVER_ADDRESS"
fi

venv/bin/coverage run --source=./tests -m pytest -xv $opts
