// returns a real number between 1 and base
// and a power of the base
// formatting can be done later

module.exports = function(base, length, value) {
  if (!value) {
    return [0, 0];
  }

  let mantissa = 1.0000 * value;
  let exponent = 0;


  while (Math.abs(mantissa) >= base) {
    mantissa = mantissa / base;
    exponent++;
  }

  while (Math.abs(mantissa) < 1) {
    mantissa = mantissa * base;
    exponent--;
  }

  // just round it off so it diplays at the right length
  mantissa = mantissa * Math.pow(base, length);
  mantissa = Math.round(mantissa);
  mantissa = mantissa / Math.pow(base, length);

  if (Math.abs(mantissa) >= base) {
    mantissa = mantissa / base;
    exponent++;
  }

  return [mantissa, exponent];
};
