const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const CopyPlugin = require('copy-webpack-plugin');

const GenerateSW = require('workbox-webpack-plugin').GenerateSW;

const options = require('./.babelrc.json');

module.exports = {
  devtool: process.env.DEVTOOL,
  entry: {
    main: __dirname + '/src/index.jsx',
  },
  output: {
    filename: '[name].js',
  },
  module: {
    rules: [{
      test: /\.jsx$/,
      use: [{
        loader: 'babel-loader',
        options: options,
      }],
      include: __dirname + '/src',
    }],
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: 'node_modules/bootstrap/dist/css/bootstrap.css',
          to: 'bootstrap.css',
        }, {
          from: 'node_modules/bootstrap/dist/css/bootstrap.css.map',
          to: 'bootstrap.css.map',
        }, {
          from: '*',
          to: '.',
          context: 'static/',
        }, {
          from: '*',
          to: '.',
          context: 'icons/',
          transform: async function(content, absoluteFrom) {
            const size = content.toString('utf8').trim();
            const ext = path.extname(absoluteFrom).replace('.', '');
            const cmd = `convert static/icon.svg -resize ${size} ${ext}:-`;
            const result = await exec(cmd, {encoding: null});
            return result.stdout;
          },
        },
      ],
    }),
    new GenerateSW({
      maximumFileSizeToCacheInBytes: 3000000,
    }),
  ],
};

if (process.env.INSTRUMENT) {
  module.exports.module.rules.push({
    test: /\.jsx?$/,
    use: [{
      loader: 'babel-loader',
      options: {
        plugins: ['istanbul'],
      },
    }],
    include: __dirname + '/src',
  });
}
