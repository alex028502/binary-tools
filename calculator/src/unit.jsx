const assert = require('assert');

const React = require('react');
const createStore = require('redux').createStore;
const mount = require('enzyme').mount;
const configure = require('enzyme').configure;

const Adapter = require('enzyme-adapter-react-16');

configure({ adapter: new Adapter() });

const App = require('./App.jsx');
const reducer = require('./reducer');
const store = createStore(reducer);

const trimArray = (array) => {
  const newArray = [];
  for (const v of array) {
    // start pushing once we have a value
    if (v || newArray.length) {
      newArray.push(v);
    }
  }
  return newArray;
};

const getStack = (wrapper) => {
  return trimArray(
    wrapper.find('textarea').get(0).props.value.split('\n')
  ).reverse();
};

const getButton = (wrapper, key) => {
  const buttons = wrapper.find('button');
  for (let idx = 0; idx < buttons.length; idx++) {
    const button = buttons.at(idx);
    if (button.text() === key) {
      return button;
    }
  }
  return null;
};

const click = (wrapper, key) => {
  getButton(wrapper, key).simulate('click');
};

const getCurrentBase = (wrapper, bases) => {
  for (const key of bases) {
    if (getButton(wrapper, key)) {
      return key;
    }
  }
  return null;
};

const switchBase = (wrapper, base) => {
  const bases = ['BIN', 'OCT', 'HEX'];
  for (const c in bases) { // just need the right number of iterations
    const b = getCurrentBase(wrapper, bases);
    if (b === base) {
      return;
    }
    click(wrapper, b);
  }
  throw new Error(`could not find base ${base}`);
};

const sut = mount(<App store={store} />);
// make sure we have mounted it:
assert(sut.contains('SNOOZE'), sut.html());

assert.deepEqual(getStack(sut), []);
assert(!getButton(sut, 'DOES NOT EXIST'));

assert.deepEqual(getStack(sut), []);

// check the errors and null in the helpers
assert.equal(getCurrentBase(sut, ['TEST']), null);
assert.throws(switchBase.bind(null, sut, 'TEST'));

// powers and stuff
// how many times do you have to double something to get eight times?
click(sut, '1');
assert.deepEqual(getStack(sut), ['1']);
click(sut, '0');
assert.deepEqual(getStack(sut), ['10']);
click(sut, 'LOG');
assert.deepEqual(getStack(sut), ['3']);
click(sut, 'AC');
assert.deepEqual(getStack(sut), []);

click(sut, '0');
assert.deepEqual(getStack(sut), ['0']);
assert(!getButton(sut, 'e^'));
click(sut, 'INV');
click(sut, 'e^');
assert.deepEqual(getStack(sut), ['1']);
click(sut, 'e^');
assertLength(getStack(sut), 1);
// now we have e, which we know starts with 2
assert.equal(getStack(sut)[0].slice(0, 2), '2.');
// actually we know it's more than two and a half:
switchBase(sut, 'HEX');
assert.equal(getStack(sut)[0].slice(0, 3), '2.i');
const hexE = getStack(sut)[0];
click(sut, '^2');
assert.equal(getStack(sut)[0].slice(0, 2), '7.'); // e^2 starts with 7
// duplicate this number for later
click(sut, 'ENTER');
assertLength(getStack(sut), 2);
assert.equal(getStack(sut)[0], getStack(sut)[1]);
assert(!getButton(sut, 'LN'));
click(sut, 'INV');
click(sut, 'LN');
assertLength(getStack(sut), 2);
assert.deepEqual(getStack(sut)[0], '2', getStack(sut));
switchBase(sut, 'BIN');
assertLength(getStack(sut), 2);
assert.deepEqual(getStack(sut)[0], 'io');
// now let's prove that e is the square root
click(sut, 'DROP');
assertLength(getStack(sut), 1);
switchBase(sut, 'HEX');
assert.equal(getStack(sut)[0].slice(0, 2), '7.');
assert.equal(getStack(sut)[0].slice(0, 2), '7.');
click(sut, 'SQRT');
assert.deepEqual(getStack(sut), [hexE]);

click(sut, 'AC');
assert.deepEqual(getStack(sut), []);

switchBase(sut, 'BIN');
click(sut, '5');
assert.deepEqual(getStack(sut), ['5']);
assert(!getButton(sut, '2^'));
click(sut, 'INV');
click(sut, '2^');
assert.deepEqual(getStack(sut), ['iooooo']);

click(sut, 'AC');
assert.deepEqual(getStack(sut), []);

// if we press snooze now nothing will happen
// because there is nothing there
click(sut, 'SNOOZE');
assert.deepEqual(getStack(sut), []);

// same with drop
click(sut, 'DROP');
assert.deepEqual(getStack(sut), []);

// factorial
switchBase(sut, 'OCT');
click(sut, '3');
assert.deepEqual(getStack(sut), ['3']);
click(sut, '!');
assert.deepEqual(getStack(sut), ['6']);

// times
click(sut, '2');
assert.deepEqual(getStack(sut), ['2', '6']);
click(sut, '*');
assert.deepEqual(getStack(sut), ['14']);
click(sut, '4');

// minus
assert.deepEqual(getStack(sut), ['4', '14']);
click(sut, '-');
assert.deepEqual(getStack(sut), ['10']);

// swap with fewer than two items
click(sut, 'SWAP');
assert.deepEqual(getStack(sut), ['10']);

// not enough numbers in stack for arity
click(sut, '*');
// just ignore it
assert.deepEqual(getStack(sut), ['10']);

function assertLength(x, length) {
  assert.equal(x.length, length, x);
}
