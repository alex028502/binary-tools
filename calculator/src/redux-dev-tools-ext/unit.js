const assert = require('assert');


const sut = require('.');

const resultWithoutExtension = sut({});
// assert not ok by asserting that not of it is true:
assert.equal(!resultWithoutExtension, true, resultWithoutExtension);

assert.equal(sut({__REDUX_DEVTOOLS_EXTENSION__: function() {
  return 'x';
}}), 'x');
