#! /usr/bin/env bash

# start a local server and run the contract test

set -e

cd $(dirname $0)

tokens=$(node tokens.js)

# export FLASK_APP=server

rm -f .coverage

venv/bin/coverage run --include=server.py server.py --without-threads &
# venv/bin/coverage run server.py --without-threads &
flask_pid=$!
sleep 2
# ps aux | awk '{ print $2 " " $10}' | grep $flask_pid

function stop-server {
  echo clean up $flask_pid if necessary
  kill $flask_pid || echo no need to clean up
}
trap stop-server EXIT

node_modules/.bin/nyc node contract.js http://localhost:5000 local $tokens

echo tests passed
echo trying to shut down server and save coverage
curl -X POST http://localhost:5000/shutdown
# kill -SIGINT $flask_pid
sleep 2
venv/bin/coverage html
venv/bin/coverage report

node_modules/.bin/nyc report --reporter=html
