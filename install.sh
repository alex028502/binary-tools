#! /usr/bin/env bash

set -e

# last time I checked, `npm ci` didn't work with -C option
# so I have to cd into each folder

cd $(dirname $0)

subprojects="$PWD/lib/fake-browser $PWD/calculator $PWD/lib/format $PWD/ip-address"

for subproject in $subprojects
do
  cd $subproject
  npm ci
done
