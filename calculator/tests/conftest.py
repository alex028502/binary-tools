def pytest_addoption(parser):
    parser.addoption("--server-address", action="store")
    # these are two slightly different concepts that I don't have good
    # names for so giving them numbers rather than risking bad names
    parser.addoption("--proxy-host-1", action="store", default="localhost")
    parser.addoption("--proxy-host-2", action="store", default="127.0.0.1")
