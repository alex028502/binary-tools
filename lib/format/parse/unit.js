const assert = require('assert');

const sut = require('.');

// const octal = '(0)0.(0)';
// const binary = '(o)o.(o)';

assert.equal(0, sut('o'));
assert.equal(1, sut('i'));
assert.equal(0, sut('0'));
assert.equal(1, sut('1'));
assert.equal(8, sut('i0'));
assert.equal(8 + 7, sut('i7'));
assert.equal(8 + 7, sut('i7'));
assert.equal(0b01010101, sut('oioioioi'));
// this is how we write hex:
assert.equal(
  0x123456789ABCDEF,
  sut('o1o2o3o4o5o6o7i0i1i2i3i4i5i6i7'),
);
// let's try fractions
assert.equal(1 + 1/8, sut('1.1'));
assert.equal(1/2, sut('0.i'));
assert.equal(1/8, sut('o.1o'));
assert.equal(1/8 + 1 / 8 / 2, sut('o.1i'));
assert.equal(2 + 1 / 8 / 8 / 8 / 8, sut('2.0001'));
// what about negative
assert.equal(0 - 8 - 7, sut('-i7'));
// and negative fractions???
assert.equal(0 - 2 - 1 / 8 / 8 / 8 / 8, sut('-2.0001'));
// what if we start with a point?
assert.equal(1 / 8, sut('.1'));
assert.equal(1 / 8, sut('0.1')); // control
