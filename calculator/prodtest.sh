#! /usr/bin/env bash

set -em

source cleanup.sh

# this tests the prod bundle
# and also tests in a subdirectory which is how it runs on the "prod" site
# so it makes sure the paths for the service worker are correct

if [[ "$SERVER_ADDRESS" == "" ]]
then
  serverargs="--bind 127.0.0.1"
  testargs="--server-address http://localhost:3000/$2"
else
  serverargs="--bind $SERVER_ADDRESS"
  testargs="--server-address http://$SERVER_ADDRESS:3000/$2"
  testargs="$testargs --proxy-host-1 $SERVER_ADDRESS"
  testargs="$testargs --proxy-host-2 $SERVER_ADDRESS"
fi

venv/bin/python -m http.server --directory $1 3000 $serverargs 2> /dev/null &

sleep 1

venv/bin/pytest -xv $testargs
