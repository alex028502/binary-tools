# Binary Calculator

## this project is about...

This project is about how cool it would be if we used binary instead of
decimal, and used octal and hex freely, but all knew that octal and hex digits
are just abbreviations for three or four binary digits respectively, and mixed
and matched them all the time, and how our reverse polish calculator might work.
\(see the [main readme}(..) for more about the number system\)

## this project is _really_ about...

This project is really about testing and continuous integration. I started out
with create react app, but then ejected, and then replaced every single piece.
I wanted to use selenium tests instead of component tests that I couldn't see.
I wanted to measure test coverage, and include selenium tests in the test
coverage "metrics", except not really "metrics", because they are always at
100%, including the tests themselves. I didn't just want to test my code, but
all the libraries I was using, to make sure that I knew what they did. I wanted
to make sure that the sevice worker and offline caching work properly by
actually turning off the server during automated tests and making sure the
app still works.

This project uses `react` and `redux` but not `react-redux`. I just don't
like all the magic. What's so bad about passing down a few variables? I know
that some people think that not "prop drilling" is the whole point of redux.
Sometimes I just pass down the whole redux store.  At least I can see what is
happening.

This project doesn't use `create-react-app`, but along with
[my other project](../ip-address), this project what a test centric react
framework could include.

## this project is a work in progress

Yeah sorry about the interface. I am still working on it. I'm not that good
at bootstrap and stuff.

Also some of the code is a bit messy some places. I don't mind. Sometimes a
really organised unit with a messy implementation work ok, especially when it's
easier to tell that you have the right answer than it is to know how to come
up with.

## this project has no ajax, or api, or server

I have [another project](../ip-address) that shows how I would test with a
server.

## this project uses python and pytest

... for selenium testing... but [my other project](../ip-address) uses js for
selenium testing...

## this project is really just a place to store a few tips and tricks

[This](./webpack.config.js) is how I configure webpack to instrument the code
when testing, and near the bottom of [this](./tests/test_main.py) you can see
a method where I save the coverage to a file after each test.

[This](./test.sh) is how I add up test coverage from selenium tests, and nyc
to make sure all code in the project is executed at least once every time we
commit.

[This](./install-mono.sh) is how I symlink to other projects in the mono repo
after `npm` has done installing external dependencies, so that `node` sees
them as packages.
