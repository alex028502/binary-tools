#! /usr/bin/env bash

set -e

# wrapper for starting proxy server with
# pytest - don't know how to tee
# or pipe

node proxy.js $1 $2 2>&1 | tee $3
