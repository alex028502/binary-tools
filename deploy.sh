#! /usr/bin/env bash

set -e

# put together directory for gitlab pages
# only one app for now, but has room for more
# github pages only seems to work if there is a directory called public

rm -rf public

mkdir public

node link.js calculator calculator >> public/index.html
node link.js ip-address ip-address >> public/index.html
convert -size 32x32 xc:#FF0000 public/favicon.ico

# this script should be used after it is already transpiled
cp -r calculator/dist/calculator public/calculator
cp -r ip-address/dist public/ip-address
# actually let's not use the randomly generated favicon
rm -f public/ip-address/favicon.ico
# now we are back to using the root one from above

