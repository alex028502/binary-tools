import subprocess
import time
import os
import uuid
import json
import signal
from urllib.parse import urlparse

import requests
import pytest
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


@pytest.fixture()
def server_address(pytestconfig):
    return pytestconfig.getoption("--server-address")


@pytest.fixture()
def constants(request):
    process = subprocess.Popen(
        ["node", "./src/constants"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout, stderr = process.communicate()
    assert not stderr
    # I bet there is a way to connect stdout directly to json.load
    return json.loads(stdout.decode("utf-8"))


def proxy_server_started(output_file):
    with open(output_file, "r") as f:
        lines = f.readlines()
        control_port = 8001
        label = "turn proxy on and off on port %s" % control_port
        # too hard to extract the ports from the output
        # let's just duplicate them here and then test
        # that they stay in sync
        for line in lines:
            if label in line:
                # it would be good to confirm that both these ports
                # match what is in the output but if we are wrong
                # we'll know soon enough
                return ["8000", control_port]
        else:
            return False


@pytest.fixture()
def proxy_server(server_address, pytestconfig):
    host = pytestconfig.getoption("--proxy-host-2")
    # thanks https://stackoverflow.com/a/32222971
    output_file = ".proxy_server_output.txt"
    env = dict(os.environ, INSTRUMENT="1", DEVTOOL="source-map")
    server_process = subprocess.Popen(
        ["./proxy.sh", split_url(server_address)[0], host, output_file],
        preexec_fn=os.setsid,
        env=env,
    )
    yield output_file
    pgrp = os.getpgid(server_process.pid)
    os.killpg(pgrp, signal.SIGINT)
    time.sleep(2)


def split_url(full_url):
    url = urlparse(full_url)
    assert url.params == "", "emtpy in every example I can find"
    if url.path:
        second_part = url.path
    else:
        second_part = "/"
    if url.query:
        second_part = second_part + "?" + url.query
    if url.fragment:
        second_part = second_part + "#" + url.fragment
    return [
        "%s://%s" % (url.scheme, url.netloc),
        second_part,
    ]


@pytest.fixture()
def proxy_server_addresses(proxy_server, server_address, pytestconfig):
    output_file = proxy_server
    for x in range(20):
        print("%s mississippi" % x)
        time.sleep(1)
        if proxy_server_started(output_file):
            break
    host = pytestconfig.getoption("--proxy-host-1")
    ports = proxy_server_started(output_file)
    assert ports
    path = split_url(server_address)[1]
    assert path[0] == "/"
    return [
        "http://%s:%s%s" % (host, ports[0], path),
        "http://%s:%s" % (host, ports[1]),
    ]


@pytest.fixture()
def browser(request):
    for y in _browser(request, None):
        yield y


@pytest.fixture()
def proxy_server_and_browser(request, proxy_server_addresses):
    for y in _browser(request, proxy_server_addresses[0]):
        yield [y] + proxy_server_addresses


def _browser(request, proxyhost):
    options = webdriver.ChromeOptions()
    options.add_argument("--ignore-certificate-errors")
    if proxyhost:
        options.add_argument(
            "--unsafely-treat-insecure-origin-as-secure=%s" % proxyhost
        )
    options.add_argument("--test-type")
    (method, kwargs) = get_driver_config(os.environ)
    driver = method(**{**kwargs, **{"options": options}})
    driver.set_window_size(600, 800)
    yield driver
    # always take a screenshot at the end so that we don't have to worry
    # about code that is only executed when tests fail, and so doesn't
    # necessarily work.
    _filename = "screenshots/%s" % request.node.name
    driver.get_screenshot_as_file("%s.png" % _filename)
    with open("%s.txt" % _filename, "w") as text_file:
        # screenshots unlike video which I prefer, don't include the address
        # bar so let's just stick it in a text file and hope we can figure out
        # what happened
        text_file.write(driver.current_url + "\n")
    driver.close()


def get_driver_config(env):
    # to make this testable on any system we return the function and arguments
    # without actually calling the function
    if env.get("SELENIUM_REMOTE_URL"):
        return (
            webdriver.Remote,
            {
                "command_executor": env["SELENIUM_REMOTE_URL"],
                "desired_capabilities": DesiredCapabilities.CHROME,
            },
        )
    return (webdriver.Chrome, {})


def test_split_url_helper():
    assert split_url("http://localhost:3000/folder") == [
        "http://localhost:3000",
        "/folder",
    ]
    assert split_url("http://localhost:3000") == [
        "http://localhost:3000",
        "/",
    ]
    assert split_url("http://1.2.3.4:3000/test") == [
        "http://1.2.3.4:3000",
        "/test",
    ]
    # we won't really use this here but whatever
    assert split_url("http://localhost:3000/folder?q=2&p=3#test") == [
        "http://localhost:3000",
        "/folder?q=2&p=3#test",
    ]


def test_setup_methods():
    # since there are things that only happen locally or only happen in ci
    # we test the methods that make that happen so that there is a reasonable
    # change that tests in one environment will help us find problems with the
    # other, and that keeps our test coverage at 100% which is a requirement
    local_env = {"test": "OK"}  # no related variables
    # SERVER_ADDRESS is no longer used by this file but it will be in
    # the environment still
    host = "example.com"
    selenium = "http://example.com:3333/ok"
    ci_vars = {"SERVER_ADDRESS": host, "SELENIUM_REMOTE_URL": selenium}
    ci_env = {**local_env, **ci_vars}
    assert get_driver_config(local_env) == (webdriver.Chrome, {})
    assert get_driver_config(ci_env) == (
        webdriver.Remote,
        {
            "command_executor": selenium,
            "desired_capabilities": DesiredCapabilities.CHROME,
        },
    )


def test_server(server_address):
    assert "3000" in server_address
    # before using selenium, let's just see if the server works
    r = requests.get(server_address)
    with open("static/index.html", "r") as f:
        assert r.text == f.read()


def get_and_handle_error(*args, **kwargs):
    try:
        return requests.get(*args, **kwargs)
    except requests.exceptions.ConnectionError:
        return False


def test_proxy_server(proxy_server_addresses):
    # let's also check the proxy server that we can
    # turn on and off to test the service worker
    with open("static/index.html", "r") as f:
        expected_content = f.read()

    # before using selenium, let's just see if the server works

    # should start with off
    r = requests.get(proxy_server_addresses[1])
    assert r.status_code == 200
    assert "ON" not in r.text
    assert "OFF" in r.text
    # so we should not be able to see anything
    r = get_and_handle_error(proxy_server_addresses[0])
    assert not r
    # now turn it on
    r = requests.post(proxy_server_addresses[1])
    assert r.status_code == 200  # I think this'll follow the redirect
    assert "ON" in r.text
    assert "OFF" not in r.text

    r = get_and_handle_error(proxy_server_addresses[0])
    assert r.status_code == 200
    assert r.text == expected_content

    # now turn it back off
    r = requests.post(proxy_server_addresses[1])
    assert r.status_code == 200  # I think this'll follow the redirect
    assert "ON" not in r.text
    assert "OFF" in r.text

    # now it should turn off instantly
    r = get_and_handle_error(proxy_server_addresses[0])
    assert not r


def try_to_load(driver, address):
    try:
        driver.get(address)
    except WebDriverException as e:
        assert "ERR_CONNECTION_REFUSED" in e.msg
        driver.execute_script("document.write('COULD NOT REACH SERVER')")


def test_going_dutch(server_address, browser):
    driver = browser
    driver.get(server_address)
    time.sleep(0.3)

    # just make sure it is loaded
    assert "button" in get_all(driver)

    # the calculator always starts in octal mode
    get_key(driver, "OCT")
    assert get_current_base(driver) == "OCT"

    # here is the bill (in an octal restaurant)
    # annotated by the three diners
    # salad $14 - all
    # coke $2 - rick
    # burger $7 - stan
    # pizza $11 - rick and phil
    # total $36

    # the diners prefer binary for stuff like this
    switch_base(driver, "BIN")
    assert get_current_base(driver) == "BIN"

    # meaning the key now says BIN
    get_key(driver, "BIN")

    # so they will type in the octal number on the bill
    # and then see it come up in binary

    # salad split three ways
    press(driver, "1")
    assert get_stack(driver) == ["1"]
    press(driver, "4")
    assert get_stack(driver) == ["14"]
    press(driver, "ENTER")
    assert get_stack(driver) == ["iioo"]
    # since these guys prefer binary, they use octal
    # when copying straight off the bill but they use
    # binary when they are punching in the number of people
    # to divide by
    press(driver, "i")
    assert get_stack(driver) == ["i", "iioo"]
    press(driver, "i")
    assert get_stack(driver) == ["ii", "iioo"]
    press(driver, "/")
    # that's cool - the binary repentation iioo / ii
    # is much easier to divide in your head by ii
    # than 14 / 3 if you are new to this stuff
    assert get_stack(driver) == ["ioo"]
    # now make three copies of the salad price!
    press(driver, "ENTER")
    assert get_stack(driver) == ["ioo", "ioo"]
    press(driver, "ENTER")
    assert get_stack(driver) == ["ioo", "ioo", "ioo"]
    # now start adding up stan
    # also had the burger
    press(driver, "7")
    assert get_stack(driver) == ["7", "ioo", "ioo", "ioo"]
    press(driver, "0")  # presses that by accident
    assert get_stack(driver) == ["70", "ioo", "ioo", "ioo"]
    press(driver, "BACK")
    assert get_stack(driver) == ["7", "ioo", "ioo", "ioo"]
    press(driver, "+")
    assert get_stack(driver) == [
        "ioii",  # stan's total
        "ioo",  # share of salad
        "ioo",  # share of salad
    ]
    # ok now snooze stan's total
    press(driver, "SNOOZE")
    assert get_stack(driver) == [
        "ioo",  # share of salad
        "ioo",  # share of salad
        "ioii",  # stan's total
    ]
    # now rick
    press(driver, "2")  # coke
    assert get_stack(driver) == ["2", "ioo", "ioo", "ioii"]
    assert get_stack(driver) == [
        "2",  # rick's coke
        "ioo",  # share of salad
        "ioo",  # share of salad
        "ioii",  # stan's total
    ]
    press(driver, "+")
    assert get_stack(driver) == [
        "iio",  # rick's subtotal
        "ioo",  # share of salad
        "ioii",  # stan's total
    ]
    # now the pizza
    press(driver, "1")
    assert get_stack(driver) == [
        "1",  # partially entered
        "iio",  # rick's subtotal
        "ioo",  # share of salad
        "ioii",  # stan's total
    ]
    press(driver, "1")
    assert get_stack(driver) == [
        "11",  # pizza
        "iio",  # rick's subtotal
        "ioo",  # share of salad
        "ioii",  # stan's total
    ]
    press(driver, "ENTER")
    assert get_stack(driver) == [
        "iooi",  # pizza
        "iio",  # rick's subtotal
        "ioo",  # share of salad
        "ioii",  # stan's total
    ]
    press(driver, "i")
    assert get_stack(driver) == [
        "i",  # partially entered
        "iooi",  # pizza
        "iio",  # rick's subtotal
        "ioo",  # share of salad
        "ioii",  # stan's total
    ]
    press(driver, "o")
    assert get_stack(driver) == [
        "io",  # two people who ate pizza
        "iooi",  # pizza
        "iio",  # rick's subtotal
        "ioo",  # share of salad
        "ioii",  # stan's total
    ]
    press(driver, "/")
    assert get_stack(driver) == [
        "ioo.i",  # half pizza
        "iio",  # rick's subtotal
        "ioo",  # share of salad
        "ioii",  # stan's total
    ]
    press(driver, "ENTER")  # duplicate that for later
    assert get_stack(driver) == [
        "ioo.i",  # half pizza
        "ioo.i",  # half pizza
        "iio",  # rick's subtotal
        "ioo",  # share of salad
        "ioii",  # stan's total
    ]
    press(driver, "SNOOZE")
    # these guys have no trouble remembering all of this as they calculate!
    # but we have to write it down to follow what they are doing
    assert get_stack(driver) == [
        "ioo.i",  # half pizza
        "iio",  # rick's subtotal
        "ioo",  # share of salad
        "ioii",  # stan's total
        "ioo.i",  # half pizza
    ]
    press(driver, "+")
    assert get_stack(driver) == [
        "ioio.i",  # rick's total
        "ioo",  # share of salad
        "ioii",  # stan's total
        "ioo.i",  # half pizza
    ]
    press(driver, "SNOOZE")
    assert get_stack(driver) == [
        "ioo",  # share of salad
        "ioii",  # stan's total
        "ioo.i",  # half pizza
        "ioio.i",  # rick's total
    ]
    press(driver, "SWAP")
    assert get_stack(driver) == [
        "ioii",  # stan's total
        "ioo",  # share of salad
        "ioo.i",  # half pizza
        "ioio.i",  # rick's total
    ]
    press(driver, "SNOOZE")
    assert get_stack(driver) == [
        "ioo",  # share of salad
        "ioo.i",  # half pizza
        "ioio.i",  # rick's total
        "ioii",  # stan's total
    ]
    press(driver, "+")
    assert get_stack(driver) == [
        "iooo.i",  # phil's total
        "ioio.i",  # rick's total
        "ioii",  # stan's total
    ]
    # now everybody knows how much to pay, but since they had to
    # keep a lot of info in their heads while they figured it out
    # let's add it up to see if it matches the total on the bill
    press(driver, "+")
    press(driver, "+")
    switch_base(driver, "OCT")
    assert get_stack(driver) == ["36"]  # same as the bill
    save_coverage(driver)


def test_service_worker(proxy_server_and_browser):
    (browser, application, control) = proxy_server_and_browser
    bit_of_page = "RAD"
    browser.get(control)
    assert "OFF" in get_all(browser)

    # now go to the app
    try_to_load(browser, application)
    assert bit_of_page not in get_all(browser)
    assert "REACH SERVER" in get_all(browser)
    time.sleep(2)

    browser.get(control)
    assert "OFF" in get_all(browser)
    browser.find_element_by_tag_name("input").click()
    assert "OFF" not in get_all(browser)

    try_to_load(browser, application)
    assert bit_of_page in get_all(browser)
    time.sleep(2)

    browser.get(control)
    assert "OFF" not in get_all(browser)
    browser.find_element_by_tag_name("input").click()
    assert "OFF" in get_all(browser)

    # still there because of caching:
    try_to_load(browser, application)
    assert bit_of_page in get_all(browser)


def test_trig(server_address, browser):
    driver = browser
    driver.get(server_address)
    time.sleep(0.3)
    assert "button" in get_all(driver)
    get_key(driver, "OCT")
    assert get_current_base(driver) == "OCT"
    assert get_stack(driver) == []
    switch_base(driver, "HEX")

    press(driver, ".")
    press(driver, "1")
    assert get_stack(driver) == [".1"]  # eighth of a turn
    press(driver, "ENTER")
    assert get_stack(driver) == ["0.o2"]
    rad_indicator = "<small>RAD</small>"
    assert rad_indicator not in get_all(driver)  # turns mode by default
    assert "ATAN" not in get_all(driver)  # we are not in INV mode
    press(driver, "TAN")
    # it's never puts 1, always a little less
    assert len(get_stack(driver)) == 1, get_stack(driver)
    assert ".i7i7i7" in get_stack(driver)[0]
    # it doesn't give quite the right answer but we have added a around off key
    # to help fix this problem
    press(driver, "ROUND")
    assert get_stack(driver) == ["1"]
    press(driver, "INV")
    assert "ATAN" in get_all(driver)
    assert "ACOS" in get_all(driver)
    assert "ASIN" in get_all(driver)
    press(driver, "RAD")
    assert rad_indicator in get_all(driver)
    press(driver, "ASIN")  # a quarter turn, or half pi
    assert len(get_stack(driver)) == 1
    # so if we divide by PI we have double the answer in turns
    press(driver, "\N{GREEK SMALL LETTER PI}")
    assert len(get_stack(driver)) == 2
    assert "3." in get_stack(driver)[0]
    press(driver, "/")
    assert len(get_stack(driver)) == 1
    # I think this might be the exact right answer
    # but let's just make sure it is close anyhow
    # assert get_stack(driver)[0][0:3] in ["0.i", "0.o"] # half
    # press(driver, "ROUND")
    assert get_stack(driver) == ["0.i"]
    press(driver, "INV")
    assert "ACOS" not in get_all(driver)
    press(driver, "RAD")
    # back in turn mode, so cosine of half will be -1
    assert rad_indicator not in get_all(driver)
    press(driver, "COS")
    assert get_stack(driver) == ["-1"]
    switch_base(driver, "BIN")
    assert get_stack(driver) == ["-i"]
    save_coverage(driver)


def test_scientific_notation(server_address, browser, constants):
    driver = browser
    driver.get(server_address)
    time.sleep(0.3)
    assert "button" in get_all(driver)
    get_key(driver, "OCT")
    assert get_current_base(driver) == "OCT"
    assert get_stack(driver) == []

    press(driver, "2")
    assert get_stack(driver) == ["2"]
    press(driver, "5")
    assert get_stack(driver) == ["25"]
    press(driver, "INV")
    press(driver, "2^")
    twoMillionOctal = "10000000"
    assert get_stack(driver) == [twoMillionOctal]
    sci_indicator = "<small>SCI</small>"
    assert sci_indicator not in get_all(driver)
    press(driver, "SCI")
    assert sci_indicator in get_all(driver)

    twoMillionOctalSci = "1.0*10^07"
    # now add in a zero until it is as wide as the screen
    assert not (constants["screenDims"]["cols"] % 2)  # should always be even
    # it always makes room for the - even if there isn't one
    while len(negString(twoMillionOctalSci)) < constants["screenDims"]["cols"]:
        twoMillionOctalSci = twoMillionOctalSci.replace("1.0", "1.00")

    assert get_stack(driver) == [twoMillionOctalSci]
    press(driver, "+/-")
    assert get_stack(driver) == [negString(twoMillionOctalSci)]
    press(driver, "SCI")
    assert sci_indicator not in get_all(driver)
    assert get_stack(driver) == [negString(twoMillionOctal)]
    press(driver, "+/-")
    assert get_stack(driver) == [twoMillionOctal]

    assert get_stack(driver) == ["10000000"]
    press(driver, "INV")
    press(driver, "LOG")
    assert get_stack(driver) == ["25"]
    press(driver, "+/-")
    assert get_stack(driver) == ["-25"]
    press(driver, "2")
    assert get_stack(driver) == ["2", "-25"]
    press(driver, "SWAP")
    assert get_stack(driver) == ["-25", "2"]
    press(driver, "^")
    press(driver, "SCI")
    assert get_stack(driver) == [twoMillionOctalSci.replace("*", "/")]
    press(driver, "1/")
    assert get_stack(driver) == [twoMillionOctalSci]
    save_coverage(driver)


def negString(s):
    return "-" + s


def switch_base(driver, base):
    while True:
        b = get_current_base(driver)
        if b == base:
            return
        press(driver, b)


def get_current_base(driver):
    for key in ["BIN", "OCT", "HEX"]:
        try:
            get_key(driver, key)
            return key
        except Exception:
            pass


def get_stack(driver):
    n = []
    for x in get_textbox(driver).get_attribute("innerHTML").split("\n"):
        if x != "" or n != []:
            n.append(x)

    return list(reversed(n))


def get_all(driver):
    return driver.find_element_by_tag_name("body").get_attribute("innerHTML")


def get_textbox(driver):
    return driver.find_element_by_tag_name("textarea")


def press(driver, key):
    get_key(driver, key).click()


def get_key(driver, key):
    return driver.find_element_by_xpath("//button[text()='%s']" % key)


def save_coverage(driver):
    coverage = driver.execute_script(
        "return JSON.stringify(window.__coverage__);"
    )
    with open(".browser/coverage%s.json" % uuid.uuid4(), "w") as text_file:
        if coverage:
            text_file.write(coverage)
