#! /usr/bin/env bash

set -e

# always run from project root
cd $(dirname $0)/..

function jsfiles {
  # include any js file that isn't gitignored
  # but to make it faster, filter out anything with node_modules first
  find . -name '*.js' | grep -vw node_modules | git check-ignore --stdin -n --verbose | grep '::' | sed 's/:://'
}

if [[ "$@" == "" ]]
then
  ARGS="--format unix"
else
  ARGS=$@
fi

# thanks https://stackoverflow.com/a/16081218
EXIT_STATUS=0
node_modules/.bin/eslint $ARGS $(jsfiles) || EXIT_STATUS=$?
! grep -wnH 'import\|export' $(jsfiles) || EXIT_STATUS=1

exit $EXIT_STATUS
