const assert = require('assert');

// const sut = require('.').bind(null, function(number) {
//   // let's use standard hex to test out the reducer to prove that it
//   // can use any parser and not just the one we are going to use
//   return parseInt(number, 0x10);
// });

// let's use decimal to parse here so that we can use this
// test to confirm things that are easy to confirm in decimal
// using existing tools
const sut = require('.').bind(null, parseFloat);

// is this how redux gets the initial state?
let state = sut(undefined, action());
assert.deepEqual(state, []);
state = sut(state, action('ENTER'));
assert.deepEqual(state, []);

// check the reducer's basic idea just to get it working
state = sut(state, {type: 'KEY', key: '2'});
assert.deepEqual(state, ['2']);
state = sut(state, {type: 'KEY', key: '1'});
assert.deepEqual(state, ['21']);
state = sut(state, action('ENTER'));
assert.deepEqual(state, ['21']);
state = sut(state, {type: 'KEY', key: '7'});
assert.deepEqual(state, ['7', '21']);
state = sut(state, action('OVER'));
assert.deepEqual(state, ['3']);

// test something with the trig functions where it would
// be too hard to confirm that we got the right answer
// in binary since we don't have _this_ tool yet:
// ok so we go by one of those signs that says 7% grade and we would like to
// know what angle of elevation that corrsponds to:
state = sut(state, action('AC'));
assert.deepEqual(state, []);
state = sut(state, {type: 'KEY', key: '.'});
assert.deepEqual(state, ['.']);
state = sut(state, {type: 'KEY', key: '0'});
assert.deepEqual(state, ['.0']);
state = sut(state, {type: 'KEY', key: '7'});
assert.deepEqual(state, ['.07']);
state = sut(state, {
  type: 'ATAN',
  rad: true,
});
assert.deepEqual(state, [`${Math.atan(0.07)}`]);

// we can test arcsine here too because the button has already been tried
state = sut(state, action('AC'));
assert.deepEqual(state, []);
state = sut(state, {type: 'KEY', key: '.'});
assert.deepEqual(state, ['.']);
state = sut(state, {type: 'KEY', key: '0'});
assert.deepEqual(state, ['.0']);
state = sut(state, {type: 'KEY', key: '7'});
assert.deepEqual(state, ['.07']);
state = sut(state, {
  type: 'ASIN',
  rad: true,
});
assert.deepEqual(state, [`${Math.asin(0.07)}`]);

// test to cover stuff that the reducer has to do for 'structural reasons'
// even though there is no way for the component to call it
state = sut(state, action('AC'));
assert.deepEqual(state, []);
state = sut(state, {type: 'KEY', key: '7'});
assert.deepEqual(state, ['7']);
state = sut(state, action('ENTER'));
assert.deepEqual(state, [7]);
state = sut(state, action('BACK'));
assert.deepEqual(state, [7]); // no change

// round off big number
// this smells a little big because even though this core can work
// with any base depending on the parser that you inject, the rounding
// off is always binary
state = sut(state, action('AC'));
assert.deepEqual(state, []);
const bigNumber = 2 * 1024 * 1024;
// this number is the round off limit
// so if we enter a number one bigger, it'll round off to this
// to do that we need to enter each digit
for (const digit of `${bigNumber + 1}`) {
  state = sut(state, {type: 'KEY', key: digit});
}
// in case you don't believe me:
assert.deepEqual(state, [bigNumber + 1]);
state = sut(state, action('ROUND'));
assert.deepEqual(state, [bigNumber]);

// the component and selenium tests make sure we click every trig
// button, and try both modes, the few corners that are left can be
// tested here

// check this out - this time give are just gonna use set the state
// like a given when then thing

// sin with rad
// GIVEN
state = [Math.PI / 2];
// WHEN
state = sut(state, {type: 'SIN', rad: true});
// THEN
assert.deepEqual(state, [1]);

// atan with turns
// GIVEN
state = [1];
// WHEN
state = sut(state, {type: 'ATAN', rad: false});
// THEN
assert.deepEqual(state, [1/8]);

// we usually don't need any arguments
function action(type) {
  return {
    type: type,
  };
}
