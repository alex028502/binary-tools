const oiDigit = require('./oi-digit.json');
const replaceAll = require('./replace-all');

module.exports = function(numberString) {
  const parts = numberString.split('.');
  if (parts.length < 2) {
    parts.push('0');
  }
  const sign = numberString[0] === '-' ? -1 : 1;
  return convertInt(parts[0] || '0') + sign * convertFraction(parts[1]);
};

function convertFraction(str) {
  return convertInt(str) / 2**expandOctal(str).length;
}

function convertInt(numberString) {
  return oiToInt(expandOctal(numberString));
}

function expandOctal(octalString) {
  const eight = 0b1000;
  let s = octalString;
  for (let i = 0; i < eight ; i++) {
    s = replaceAll(
      s,
      i.toString(eight),
      pad(intToOi(i)),
    );
  }
  return s;
}

function intToOi(integer) {
  let s = integer.toString(2);
  for (let k in oiDigit) {
    s = replaceAll(s, k, oiDigit[k]);
  }
  return s;
}

function pad(unpadded) {
  let padded = unpadded;
  while (padded.length < 3) {
    padded = oiDigit[0b0] + padded;
  }
  return padded;
}

function oiToInt(oi) {
  let s = oi;
  for (let k in oiDigit) {
    s = replaceAll(s, oiDigit[k], k);
  }
  return parseInt(s, 2);
}
