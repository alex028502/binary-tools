const oiDigit = require('./parse/oi-digit.json');
const replaceAll = require('./parse/replace-all');
const scientific = require('./scientific');

const baseValue = {
  bin: 0b10,
  oct: 0x8,
  hex: 0x10,
};

const format = {
  bin: function(value) {
    let s = value.toString(baseValue['bin']);
    for (const k in oiDigit) {
      s = replaceAll(s, k, oiDigit[k]);
    }
    return s;
  },
  oct: function (value) {
    return value.toString(baseValue['oct']);
  },
  hex: function (value) {
    let s = value.toString(0x10).split('').map(function(x) {
      if ('.-'.indexOf(x) + 1) {
        return x;
      }
      const v = parseInt(x, 0x10);
      if (v < 0x8) {
        return 'o' + v;
      } else {
        return 'i' + (v - 0x8);
      }
    }).join('');
    if (s[0] === 'o' || s.slice(0, 2) === '-o') {
      s = s.replace('o', '');
    }
    while (s.indexOf('.') + 1 && '0o.'.indexOf(s[s.length -1]) + 1) {
      s = s.slice(0, -1);
    }
    return s;
  },
};

module.exports = Object.assign({}, format, {sci: sci});

// these are only for scientific notation
const baseString = {
  bin: 'io',
  oct: '10',
  hex: '20', // written in octal on calculator
};

function sci(baseName, length, value) {
  // possible minus sign 1
  // first digit 1
  // separatrix 1
  // asterisk 1
  // base 2
  // caret 1
  // power 2
  const extraChars = 0x9;
  let fractionLength = length - extraChars;
  if (baseName === 'hex') {
    // because there are two digits before the fraction
    // and then each fraction place needs two digits
    fractionLength = (fractionLength - 1) / 2;
  }
  const [mantissa, exponent] = scientific(
    baseValue[baseName],
    fractionLength,
    value,
  );

  const x = (exponent < 0) ? '/' : '*';
  // console.log('formatting', mantissa, exponent);
  let mantissaStr = format[baseName](mantissa);
  if (mantissaStr.indexOf('.') === -1) {
    mantissaStr += '.';
  }

  let expStr = format.oct(Math.abs(exponent));
  if (expStr.length < 2) {
    expStr = '0' + expStr;
  }

  if (baseName === 'hex' && 'io'.indexOf(mantissaStr.slice(-1)) + 1) {
    mantissaStr += '0';
  }

  if (baseName === 'hex') {
    if (mantissaStr.slice(0, 1) === 'i') {
      // we are good
    } else if (mantissaStr.slice(0, 2) === '-i') {
      // we are good
    } else if (mantissaStr.slice(0, 1) === '-') {
      mantissaStr = mantissaStr.replace('-', '-o');
    } else {
      mantissaStr = 'o' + mantissaStr;
    }
  }

  const targetLength = mantissa < 0 ? length : length - 1;
  for(;;) {
    const answer = `${mantissaStr}${x}${baseString[baseName]}^${expStr}`;
    if (answer.length >= targetLength) {
      return answer;
    }
    if (baseName === 'bin') {
      mantissaStr += 'o';
    }
    if (baseName === 'oct') {
      mantissaStr += '0';
    }
    if (baseName === 'hex') {
      mantissaStr += 'o0';
    }
  }
}
