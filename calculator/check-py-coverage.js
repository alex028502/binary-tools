// command line tool to make sure coverage is 100%
// written in js ironically - parsing json is easier

const assert = require('assert');

const coverageFile = process.argv[2];

console.log(`checking for 100% coverage in ${coverageFile}`);

const coverageInfo = require(coverageFile);

assert.equal(coverageInfo.totals.percent_covered, 100);
assert.equal(coverageInfo.totals.missing_lines, 0);

console.log(`coverage in ${coverageFile} is good!`);
