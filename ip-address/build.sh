#! /usr/bin/env bash

set -e

config_file=vars/$1.json

rm -rf $2
mkdir $2

# let's make a random favicon!
function get-color {
  # for each color we are going to get a random number up to 1o0o0 from our
  # random number tool, and then convert it to decimal with another tool
  # and then convert that into the standard 0123456789ABCDEF format
  hex=$(node random-color/index.js)
  dec=$(node node_modules/@monorepo/format/to-decimal.js $hex)
  printf '%02x\n' $dec
}

red=$(get-color)
green=$(get-color)
blue=$(get-color)

randomxc=$red$green$blue
echo making favicon $randomxc this time

convert -size 16x16 xc:#$randomxc $2/favicon.ico

node_modules/.bin/mustache $config_file src/index.tpl.html > $2/index.html
node_modules/.bin/webpack build --mode=$1 -o $2

