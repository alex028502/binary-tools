const config = {
  devtool: 'source-map',
  entry: {
    index: `${__dirname}/src/index.js`,
  },
  output: {
    filename: '[name].js',
  },
  module: {
    rules: [],
  },
};

module.exports = function (env, argv) {
  if (argv.mode === 'development') {
    config.devtool = 'cheap-source-map';
    config.module.rules.push({
      test: /\.js$/,
      use: [{
        loader: 'babel-loader',
        options: {
          plugins: ['istanbul'],
        },
      }],
      include: `${__dirname}/src`,
    });
  }

  return config;
};
