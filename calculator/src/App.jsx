const React = require('react');
const useForceUpdate = require('use-force-update').default;

const Container = require('react-bootstrap').Container;
const Row = require('react-bootstrap').Row;
const Col = require('react-bootstrap').Col;
const Button = require('react-bootstrap').Button;

const format = require('@monorepo/format');
const parse = require('@monorepo/format/parse');
const screenDims = require('./constants').screenDims;

const App = (props) => {
  const forceUpdate = useForceUpdate();
  const store = props.store;
  store.subscribe(forceUpdate);
  return (
    <Container fluid>
      <Row>
        <Col>
          { store.getState().mode.rad ? <small>RAD</small> : <div>&nbsp;</div> }
        </Col>
        <Col>
          { store.getState().mode.sci ? <small>SCI</small> : <div>&nbsp;</div> }
        </Col>
        <Col lg="4" md="3" sm="2" xl="5" xs="1"><div>&nbsp;</div></Col>
      </Row>
      <Row>
        <Col>
          <Screen registers={store.getState().core}
                  rows={screenDims.rows}
                  cols={screenDims.cols}
                  sci={store.getState().mode.sci}
                  base={store.getState().mode.base} />
        </Col>
      </Row>
      <Row>
        <Col>
          <KeyPad store={store} />
        </Col>
      </Row>
    </Container>
  );
};

const KeyPad = (props) => {
  // const store = props.test?.ready ? props.test : props.store;
  const store = props.store;
  return (
    <div>
      <ButtonGroup>
        <KeyOp store={store} op={() => "AC"}>AC</KeyOp>
        <KeyOp store={store} op={() => "SNOOZE"}>SNOOZE</KeyOp>
        <KeyOp store={store} op={() => "SWAP"}>SWAP</KeyOp>
        <KeyBackDrop store={store} />
      </ButtonGroup>
      <ButtonGroup>
        <KeyOp store={store} op={() => "MODE_INV"}>INV</KeyOp>
        <KeyOp store={store} op={() => "MODE_RAD"}>RAD</KeyOp>
        <KeyOp store={store} op={() => "MODE_BASE"}>
          {store.getState().mode.base.toUpperCase()}
        </KeyOp>
        <KeyOp store={store} op={() => "MODE_SCI"}>SCI</KeyOp>
      </ButtonGroup>
      <ButtonGroup>
        <KeyOp store={store} op={() => "FACTORIAL"}>!</KeyOp>
        <KeyOp store={store} op={() => "TO"}>^</KeyOp>
        <KeySqrt store={store} />
        <KeyOp store={store} op={() => "1/"}>1/</KeyOp>
      </ButtonGroup>
      <ButtonGroup>
        <KeyTrig store={store} f={() => "SIN"}>SIN</KeyTrig>
        <KeyTrig store={store} f={() => "COS"}>COS</KeyTrig>
        <KeyTrig store={store} f={() => "TAN"}>TAN</KeyTrig>
        <KeyOp store={store} op={() => "PI"}>&pi;</KeyOp>
      </ButtonGroup>
      <ButtonGroup>
        <KeyOp store={store} op={() => "ROUND"}>ROUND</KeyOp>
        <KeyLn store={store} />
        <KeyLog store={store} />
        <KeyOp store={store} op={() => "NEGATE"}>+/-</KeyOp>
      </ButtonGroup>
      <ButtonGroup>
        <KeyChar store={store}>7</KeyChar>
        <KeyChar store={store}>i</KeyChar>
        <KeyChar store={store}>o</KeyChar>
        <KeyOp store={store} op={() => "OVER"}>/</KeyOp>
      </ButtonGroup>
      <ButtonGroup>
        <KeyChar store={store}>4</KeyChar>
        <KeyChar store={store}>5</KeyChar>
        <KeyChar store={store}>6</KeyChar>
        <KeyOp store={store} op={() => "TIMES"}>*</KeyOp>
      </ButtonGroup>
      <ButtonGroup>
        <KeyChar store={store}>1</KeyChar>
        <KeyChar store={store}>2</KeyChar>
        <KeyChar store={store}>3</KeyChar>
        <KeyOp store={store} op={() => "MINUS"}>-</KeyOp>
      </ButtonGroup>
      <ButtonGroup>
        <KeyChar store={store} >0</KeyChar>
        <KeyChar store={store} >.</KeyChar>
        <KeyOp store={store} op={() => "ENTER"}>ENTER</KeyOp>
        <KeyOp store={store} op={() => "PLUS"}>+</KeyOp>
      </ButtonGroup>
    </div>
  );
};

const Screen = (props) => {
  const rows = props.rows;
  const cols = props.cols;
  const sciFunction = format.sci.bind(null, props.base, cols);
  let formatFunction;
  if (props.sci) {
    formatFunction = sciFunction;
  } else {
    formatFunction = (x) => {
      const xs = format[props.base](x);
      if (xs.length > cols) {
        return sciFunction(x);
      }
      return xs;
    };
  }
  let values = props.registers.map(function(x) {
    if (typeof(x) !== 'string') {
      return formatFunction(x);
    }
    return x;
  }).slice(0, rows);
  while (values.length < rows) {
    values = values.concat(['']);
  }
  return <textarea rows={rows}
                   readOnly
                   style={{textAlign: 'end', width: '100%'}}
                   value={values.reverse().join('\n')} />;
};

const KeySqrt = (props) => {
  if (!props.store.getState().mode.inv) {
    return <KeyOp store={props.store} op={() => 'SQRT'}>SQRT</KeyOp>;
  }
  return <KeyOp store={props.store} op={() => '^2'}>^2</KeyOp>;
};

const KeyLn = (props) => {
  if (!props.store.getState().mode.inv) {
    return <KeyOp store={props.store} op={() => 'LN'}>LN</KeyOp>;
  }
  return <KeyOp store={props.store} op={() => 'e^'}>e^</KeyOp>;
};

const KeyLog = (props) => {
  if (!props.store.getState().mode.inv) {
    return <KeyOp store={props.store} op={() => 'LOG'}>LOG</KeyOp>;
  }
  return <KeyOp store={props.store} op={() => '2^'}>2^</KeyOp>;
};

const KeyTrig = (props) => {
  const inv = props.store.getState().mode.inv;
  const rad = props.store.getState().mode.rad;
  return (
    <Key onClick={function () {
      props.store.dispatch({
        type: inv ? `A${props.f()}` : props.f(),
        rad: rad,
      });
    }}>{inv ? `A${props.children}` : props.children}</Key>
  );
};

const KeyChar = (props) => {
  return (
    <Key onClick={character.bind(null, props.store, props.children[0])}>
    { props.children }
    </Key>
  );
};

const character = (store, ch) => {
  store.dispatch({
    type: 'KEY',
    key: ch,
  });
};

const KeyBackDrop = (props) => {
  // we could figure this out in the core, but we want to display something
  // different on the button, so we can work it out here too
  // it just means that there are code paths in the reducer that could never
  // be used by the full app
  if (editing(props.store)) {
    return <KeyOp store={props.store} op={() => "BACK"}>BACK</KeyOp>;
  }
  return <KeyOp store={props.store} op={() => "DROP"}>DROP</KeyOp>;
};

const editing = (store) => {
  if (!store.getState().core.length) {
    return false;
  }

  return typeof(store.getState().core[0]) === 'string';
};

const KeyOp = (props) => {
  return (
    <Key onClick={operation.bind(null, props.store, props.op)}>
      { props.children }
    </Key>
  );
};

const operation = (store, op) => {
  store.dispatch({
    type: op(),
  });
};

const ButtonGroup = (props) => {
  return (
    <Row noGutters>
      {props.children}
    </Row>
  );
};

const Key = (props) => {
  return (
    <Col>
      <Button onClick={props.onClick} block>
        {props.children}
      </Button>
    </Col>
  );
};

module.exports = App; // don't try to inline this react-hooks/rules-of-hooks
