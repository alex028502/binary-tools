const format = require('@monorepo/format');

module.exports = function (ipaddress) {
  return ipaddress.split('.').map(function(n) {
    return parseInt(n);
  }).map(function(x) {
    return format.hex(x);
  }).map(function(x) {
    const padding = 'o0o0';
    const missingLength = padding.length - x.length;
    return padding.substring(0, missingLength) + x;
  }).join(' ');
};
