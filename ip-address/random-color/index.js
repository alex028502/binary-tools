/* eslint no-console: 0 */

const hex = require('@monorepo/format').hex;
const parse = require('@monorepo/format/parse');

const value = Math.floor((Math.random() * parse('1o0o0')));

console.log(hex(value));
