#! /usr/bin/env bash

set -e

cd $(dirname $0)

echo testing random number maker by injecting non random numbers

set -x
[[ $(node test.js 0.5) == i0o0 ]]
[[ $(node test.js 0.25) == 4o0 ]]
[[ $(node test.js 0.75) == i4o0 ]]
set +x

echo seems to work

