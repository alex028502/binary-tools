# ip address project

This project shows your ip address using the hexidecimal number format described
[here](..)

However, it is really a way to store a few more CI and testing tricks that are
not covered in the [calculator](../calculator) project:

- using javascript selenium
- using a [fake api](./api) for [development](./serve.sh) and
[testing](./selenium.sh) so that the dev server and dev api are started as soon
as you type `npm start` or `npm test`
