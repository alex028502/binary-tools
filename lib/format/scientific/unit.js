const assert = require('assert');

const sut = require('.');

// the mantissa gets rounded off here, but trailing 0s still have to be put
// on later since it returns a float
assert.deepEqual([1.345, 0], sut(10, 3, 1.345));
assert.deepEqual([1.345, 2], sut(10, 3, 134.5));
assert.deepEqual([1.345, -2], sut(10, 3, 0.01345));
assert.deepEqual([1.34500, 2], sut(10, 5, 134.5));
// which is really the same as this:
assert.deepEqual([1.345, 2], sut(10, 5, 134.5));

assert.deepEqual([1.35, 2], sut(10, 2, 134.5));
assert.deepEqual([-1.34500, 2], sut(10, 5, -134.5));
assert.deepEqual([0, 0], sut(10, 2, 0));

assert.deepEqual([1, 0], sut(10, 1, 0.999));

// we used tens for testing of this component because of how the programming
// language writes floats, but we can do a couple with some of our bases too

assert.deepEqual([13.5, 0], sut(0x10, 7, 0xD + 0.5));
assert.deepEqual([1.0 + 1.0 / 0x10, 1], sut(0x10, 7, 0x11));

assert.deepEqual([15 + 15 / 16 + 15 / 256, -1], sut(0x10, 4, 0xFFF / 0x1000));
assert.deepEqual([15 + 15 / 16 + 15 / 256, -1], sut(0x10, 2, 0xFFF / 0x1000));
assert.deepEqual([1, 0], sut(0x10, 1, 0xFFF / 0x1000));
