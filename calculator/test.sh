#! /usr/bin/env bash

set -e

cd $(dirname $0)

function explain-coverage {
  echo ^^^^^^^ coverage for $@ ^^^^^^^
}

rm -rf .nyc_* .browser

# TODO should we create these empty folder in python
rm -rf coverage screenshots
mkdir .browser
mkdir screenshots
node_modules/.bin/nyc ./selenium.sh
explain-coverage long story
mv .nyc_output .nyc_selenium

# make sure we have the screenshots - we really need the screenshot when there
# is a failure, but by checking for it on success, we can help make sure it is
# there when we need it
ls screenshots/test_*.png screenshots/test_*.txt > /dev/null
# there is only one url for the whole test so that url should be recorded in
# this file at the end of the test:
cat screenshots/test_*.txt | grep "3000" > /dev/null
cat screenshots/test_*.txt | grep "http" > /dev/null

venv/bin/coverage report
explain-coverage selenium test code

node_modules/.bin/nyc report -t .browser
explain-coverage js during selenium tests

node_modules/.bin/nyc -t .nyc_unit ./unit.sh
explain-coverage unit tests

node_modules/.bin/nyc -t .nyc_component ./component.sh
explain-coverage component tests

mkdir .nyc_output
cp -r .nyc_component/* .nyc_output
cp -r .nyc_unit/* .nyc_output
cp -r .nyc_selenium/* .nyc_output
cp -r .browser/* .nyc_output
node_modules/.bin/nyc report
explain-coverage all js

node_modules/.bin/nyc report --reporter=html --reporter=cobertura
venv/bin/coverage html
venv/bin/coverage xml
echo checking for 100% js code coverage
echo if this fails see $PWD/coverage/index.html in your browser for the details
node_modules/.bin/nyc check-coverage --lines 100 --branches 100 --functions 100 --statements 100

venv/bin/coverage report
explain-coverage all python
echo checking for 100% python code coverage
echo if this fails see $PWD/htmlcov/index.html in your browser for the details
# couldn't find a way to check for minimum coverage
# TODO replace this whenever we figure out the built in way
venv/bin/coverage json
node check-py-coverage.js ./coverage.json
# now let's just output something that matches the parser we selected
# since we only pass builds with 100% coverage
echo [TOTAL] 100.0%
