const parse = require('@monorepo/format/parse');

// this could have been just a json file except that json doesn't allow hex and
// we want to avoid decimal values, so instead we have a js file that can be
// imported by the app, and using the python trick, can be executed too so that
// python can get the info as json

module.exports = {
  screenDims: {
    rows: parse('10'),
    cols: parse('50'),
  },
};

// thanks https://stackoverflow.com/a/6090287
if (require.main === module) {
  console.log(JSON.stringify(module.exports));
}
