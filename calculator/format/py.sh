#! /usr/bin/env bash

set -e

# always run from project root
cd $(dirname $0)/..

function files {
  # any python file not in venv
  find . -name '*py' | grep -v venv | grep -v node_modules
}

files | xargs venv/bin/flake8 --max-line-length 80 || echo but first let\'s consult black

files | xargs venv/bin/black -t py37 -l 79 $@

# now let's see if that got everything
# but since black doesn't always succeed in shortening the lines
# let's forgive up to 88 characters
# so there are only a few rules that this will catch that won't
# be fixed by black (variable names)
files | xargs venv/bin/flake8 --max-line-length 88
