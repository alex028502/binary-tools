

--------- IN CASE OF FORMAT CHECK FAILURES ----------
javascript is format checked with eslint and some custom searches
the custom search failures just look like grep search results
they are for js keywords that we have decided not to use

python is format checked with black

if the js format check fails, you can try to fix it like this

format/js.sh --fix

if there are still issues that need to be fixed manually, they will be listed
to list everything in an IDE friendly format, go like this
format/js.sh
to list this eslint stuff in a human friendly format, go like this
format/js.sh --format stylish

if the python format check fails, you will usually be able to fix it with
format/py.sh
black doesn't give you the details about why it failed
but we check with flake8 before and after black to show us stuff that
black can't fix and to show us some of what black is fixing
-----------------------------------------------------

