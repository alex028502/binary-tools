
module.exports = function(parse, state=[], action) {
  switch (action.type) {
  case 'DROP':
    if (!state.length) {
      return state;
    }
    return state.slice(1);
  case 'SNOOZE':
    if (!state.length) {
      return state;
    }
    return state.slice(1).concat(state[0]);
  case 'SWAP':
    if (state.length < 2) {
      return state;
    }
    return [state[1], parseIfNeeded(parse, state[0])].concat(state.slice(2));
  case 'AC':
    return [];
  case 'KEY':
    if (state.length && typeof state[0] === 'string') {
      return [state[0] + action.key].concat(state.slice(1));
    } else {
      return [action.key].concat(state);
    }
  case 'ENTER':
    if (!state.length) {
      // it will be showing zero but nothing happens
      return state;
    }
    if (typeof(state[0]) === 'string') {
      // finalize the string
      return [parse(state[0])].concat(state.slice(1));
    } else {
      // repeat
      return [state[0]].concat(state);
    }
  case 'BACK':
    if (typeof(state[0]) === 'string' && state[0].length) {
      // finalize the string
      return [state[0].slice(0, -1)].concat(state.slice(1)).filter(function(x) {
        return x;
      });
    }
    // if we are not editing, just ignore
    // you have to use DROP if you want to remove the last entry
    return state;
  case 'PLUS':
    return operation(parse, state, function (a, b) {
      return a + b;
    }, 2);
  case 'MINUS':
    return operation(parse, state, function (a, b) {
      return a - b;
    }, 2);
  case 'TIMES':
    return operation(parse, state, function (a, b) {
      return a * b;
    }, 2);
  case 'OVER':
    return operation(parse, state, function (a, b) {
      return a / b;
    }, 2);
  case 'NEGATE':
    return operation(parse, state, function (a) {
      return -a;
    }, 1);
  case 'SQRT':
    return operation(parse, state, Math.sqrt, 1);
  case 'TO':
    return operation(parse, state, Math.pow, 2);
  case 'PI':
    return operation(parse, state, always.bind(null, Math.PI), 0);
  case 'FACTORIAL':
    return operation(parse, state, factorial, 1);
  case 'e^':
    return operation(parse, state, Math.pow.bind(null, Math.E), 1);
  case '^2':
    return operation(parse, state, function (a) {
      return a * a;
    }, 1);
  case 'LN':
    return operation(parse, state, Math.log, 1);
  case '2^':
    return operation(parse, state, Math.pow.bind(null, 2), 1);
  case '1/':
    return operation(parse, state, function(x) {
      return 1 / x;
    }, 1);
  case 'LOG':
    return operation(parse, state, function (x) {
      return Math.log(x) / Math.log(2);
    }, 1);
  case 'SIN':
  case 'COS':
  case 'TAN':
    return operation(parse, state, function (x) {
      const arg = action.rad ? x : (x * 2 * Math.PI);
      return Math[action.type.toLowerCase()](arg);
    }, 1);
  case 'ASIN':
  case 'ACOS':
  case 'ATAN':
    return operation(parse, state, function (x) {
      const result = Math[action.type.toLowerCase()](x);
      return action.rad ? result : result / 2 / Math.PI;
    }, 1);
  case 'ROUND':
    // enough significant digits to count to a million or a millionth
    return operation(parse, state, function (x) {
      const million = 1024 * 1024;
      let mantissa = x;
      let order = 0;
      while (mantissa > million) {
        mantissa = mantissa / 2;
        order++;
      }
      while (mantissa < million / 2) {
        mantissa = mantissa * 2;
        order--;
      }
      mantissa = Math.round(mantissa);
      return Math.pow(2, order) * mantissa;
    }, 1);
  default:
    return state;
  }
};

function always(x) {
  return x;
}

function factorial(a) {
  let total = 1;
  for (let n = 2; n <= a; n++) {
    total = total * n;
  }
  return total;
}

function parseIfNeeded(parse, value) {
  return (typeof(value) === 'string') ? parse(value) : value;
}

function operation(parse, state, f, arity) {
  if (state.length < arity) {
    return state;
  }

  return operate(
    state.map(parseIfNeeded.bind(null, parse)),
    f,
    arity,
  );
}

function operate(state, f, arity) {
  return [f.apply(null, state.slice(0, arity).reverse())].concat(
    state.slice(arity),
  );
}
