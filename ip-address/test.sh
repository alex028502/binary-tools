#! /usr/bin/env bash

set -e

c=.coverage
rm -rf $c coverage
mkdir $c

node_modules/.bin/nyc ./selenium.sh

echo generated the following coverage files
ls $c/*.json

node_modules/.bin/nyc report -t $c

mv .nyc_output/*.json $c

node_modules/.bin/nyc ./unit.sh

mv .nyc_output/*.json $c

node_modules/.bin/nyc report -t $c
node_modules/.bin/nyc report -t $c -r html

node_modules/.bin/nyc check-coverage -t $c --lines 100 --branches 100 --functions 100 --statements 100

echo tests pass and everything seems to be covered
