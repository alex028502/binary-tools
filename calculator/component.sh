#! /usr/bin/env bash

set -e

cd $(dirname $0)

for filename in $(find . src -name unit.jsx)
do
  node -r ./jsx-test $filename
done
