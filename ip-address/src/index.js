/* eslint-env node, browser */

const convert = require('./convert');

const relativePort = require('./relative-port');

(async function () {
  const div = document.getElementsByClassName('ip-address')[0];
  const api = relativePort(
    URL,
    div.getAttribute('api'),
    window.location.href,
  );
  try {
    const response = await fetch(api);
    if (!response.ok) {
      throw new Error(response.status);
    }
    const result = await response.json();
    const answer = convert(result.ip);
    write(div, answer);
  } catch(e) {
    write(div, 'ERROR');
  }
})();

function write(div, message) {
  div.innerHTML = message;
}
