module.exports = function(string, before, after) {
  let s = string;
  while (s.indexOf(before) + 1) {
    s = s.replace(before, after);
  }
  return s;
};

