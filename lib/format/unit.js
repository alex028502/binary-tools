const assert = require('assert');

const sut = require('.');
const sci = sut.sci;
const parse = require('./parse');

assert.equal(sut.bin(0b0), 'o');
assert.equal(sut.oct(0b0), '0');
assert.equal(sut.hex(0b0), '0');

assert.equal(sut.bin(0b1), 'i');
assert.equal(sut.oct(0b1), '1');
assert.equal(sut.hex(0b1), '1');

assert.equal(sut.bin(0x10), 'ioooo');
assert.equal(sut.oct(0x10), '20');
assert.equal(sut.hex(0x10), '1o0');

assert.equal(sut.bin(0x11), 'ioooi');
assert.equal(sut.oct(0x11), '21');
assert.equal(sut.hex(0x11), '1o1');

// fractions
assert.equal(sut.bin(0x111 + 0x111/0x1000), 'ioooioooi.oooioooioooi');
assert.equal(sut.oct(0x111 + 0x111/0x1000), '421.0421');
assert.equal(sut.hex(0x111 + 0x111/0x1000), '1o1o1.o1o1o1');
assert.equal(sut.hex(1/2), '0.i');

// negative
assert.equal(sut.bin(-0x11), '-ioooi');
assert.equal(sut.oct(-0x11), '-21');
assert.equal(sut.hex(-0x11), '-1o1');

// negative fractions
assert.equal(
  sut.bin(-0x111 - 0x111/0x1000),
  '-ioooioooi.oooioooioooi',
);
assert.equal(sut.oct(-0x111 - 0x111/0x1000), '-421.0421');
assert.equal(sut.hex(-0x111 - 0x111/0x1000), '-1o1o1.o1o1o1');

// check it all works together
const complicatedNumber = -0x111 - 0x111/0x1000;
assert.equal(complicatedNumber, parse(sut.bin(complicatedNumber)));
assert.equal(complicatedNumber, parse(sut.oct(complicatedNumber)));
assert.equal(complicatedNumber, parse(sut.hex(complicatedNumber)));

// anything shorter than a single digit fraction is undefined behaviour
// the view layer will have more space than that so it doesn't matter
assert.equal('i.o*io^04', sci('bin', 0xA, 0x10));
assert.equal('i.oo*io^10', sci('bin', 0xB, 0x111 + 0x111/0x1000));

// round off
assert.equal(sci('bin', 0xC, 0x111 + 0x111/0x1000), 'i.ooi*io^10');
// octal
assert.equal(sci('oct', 0xA, 0x111 + 0x111/0x1000), '4.2*10^02');
assert.equal(sci('oct', 0xB, 0x111 + 0x111/0x1000), '4.21*10^02');
// hex - the base is written in octal to keep the width consistent in all bases
assert.equal(sci('hex', 0xE, 0x111 + 0x111/0x1000), 'o1.o1o1*20^02');

// negative exponents - we use / instead
assert.equal(sci('bin', 0x11, 0x111/0x1000), 'i.oooioooi/io^04');
assert.equal(sci('oct', 0xB, 0x111/0x1000), '4.21/10^02');
assert.equal(sci('hex', 0xE, 0x111/0x1000), 'o1.o1o1/20^01');

// truncate
assert.equal(sci('bin', 0x10, 0x111/0x1000), 'i.oooiooi/io^04');
assert.equal(sci('oct', 0xA, 0x111/0x1000), '4.2/10^02');
assert.equal(sci('hex', 0xC, 0x111/0x1000), 'o1.o1/20^01');
// hex always come in pairs
assert.equal(sci('hex', 0xC, 0x111/0x1000), 'o1.o1/20^01');
assert.equal(sci('hex', 0xE, 0xFFF/0x1000), 'i7.i7i7/20^01');
assert.equal(sci('hex', 0xC, 0xFFF/0x1000), 'o1.o0*20^00');
// hex gets padded with the right stuff
assert.equal(sci('hex', 0x12, -1), '-o1.o0o0o0o0*20^00');
assert.equal(sci('hex', 0x12, -0xF), '-i7.o0o0o0o0*20^00');
assert.equal(sci('hex', 0x12, -0x11), '-o1.o1o0o0o0*20^01');
// two digit exponent
assert.equal(sci('hex', 0xD, 0xF / 0x100000000), 'i7.o0o0/20^10');
assert.equal(sci('hex', 0xD, 0xF / 0x1000000000), 'i7.o0o0/20^11');
// when padding the right starts half way through hex digit:
assert.equal(sci('hex', 0xD, 0x18 / 0x10), 'o1.i0o0*20^00');
// same thing in oct (only that means it's totally different)
assert.equal(sci('oct', 0xE, 0x18 / 0x10), '1.40000*10^00');
