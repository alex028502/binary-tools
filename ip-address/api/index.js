/* eslint no-console: 0 */

const express = require('express');
const cors = require('cors');

const app = express();

const getHostFromEnv = require('./get-host-from-env');

app.use(cors());

const port = process.argv[2];

let response = '192.168.1.1';

// this is just a test server
// totally not perfect
// allows us to set the response

app.get('/', function (req, res) {
  if (response == '500') {
    throw new Error('unexpected');
    // we could send the error
    // but let's just have an error
  }
  if (response.indexOf('.') + 1) {
    // ip address
    res.send({ip: response});
  } else {
    // if there are not dots in the number
    // then it's an http status
    res.sendStatus(parseInt(response));
    res.send('happy birthday');
  }
});

app.post('/', function (req, res) {
  // post request but not using payload!
  response = req.query.response;
  res.send(`response set to ${response}`);
});

console.log(`starting api on ${port}`);
app.listen(port, getHostFromEnv(process.env), function() {
  console.log(`api running on http://localhost:${port}`);
});
