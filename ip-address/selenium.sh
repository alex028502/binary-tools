#! /usr/bin/env bash

set -e

echo will start a server and then run some tests

set -m # don't try to stop an npm script without this!
# NOTE I think I have a better way to do this now - the problem is
# that now this script is in a _different_ group the processes it starts
# so if you ctrl+c _this_ script, you are dependent on the cleanup script
# working properly - in newer projects I am working on just tracking down
# every child process from the test wrapper as a way of simulating ctrl+c


npm start &
server_pid=$!

function cleanup {
  echo closing process group $server_pid
  kill -SIGINT -$server_pid || echo nothing to close
}
trap cleanup EXIT

max_tries=XXXXXXX
tries=

while ! curl -fv http://localhost:5000/index.js
do
  tries="X$tries"
  set -x
  [[ "$tries" != "$max_tries" ]]
  set +x
  echo wait a moment before trying again
  sleep 3
done

./node_modules/.bin/mocha selenium.js --timeout 5000
