const combineReducers = require('redux').combineReducers;

const parse = require('@monorepo/format/parse');

module.exports = combineReducers({
  core: require('./core').bind(null, parse),
  mode: mode,
});

function mode(state={base: 'oct'}, action) {
  switch(action.type) {
  case 'MODE_BASE':
    const bases = ['bin', 'oct', 'hex'];
    return Object.assign({}, state, {
      base: bases[(bases.indexOf(state.base) + 1) % 3],
    });
  case 'MODE_INV':
    return Object.assign({}, state, {
      inv: !state.inv,
    });
  case 'MODE_RAD':
    return Object.assign({}, state, {
      rad: !state.rad,
    });
  case 'MODE_SCI':
    return Object.assign({}, state, {
      sci: !state.sci,
    });
  default:
    return state;
  }
}
