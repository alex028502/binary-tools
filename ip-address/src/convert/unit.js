const assert = require('assert');

const parse = require('@monorepo/format/parse');
const replaceAll = require('@monorepo/format/parse/replace-all');

const sut = require('.');

assert.equal(sut('0.0.0.0'), 'o0o0 o0o0 o0o0 o0o0');

const digits = [1, 1, 168, 192];
let value = 0;
for (const i in digits) {
  value += digits[i] * (4 ** (4 * i));
}
digits.reverse(); // mutation!!
const ipAddressString = digits.join('.');

assert.equal(
  parse(replaceAll(sut(ipAddressString), ' ', '')),
  value,
);
