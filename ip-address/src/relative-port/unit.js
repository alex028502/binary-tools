const assert = require('assert');

const sut = require('.').bind(null, require('url').URL);

// calculate a relative path that starts with the port
// https://stackoverflow.com/questions/6016120
// it seems like the browser won't handle it

// absolute paths go straight through
assert.equal(
  sut('https://test.com/ok', 'https://example.com/test'),
  'https://test.com/ok',
);

// any relative path that the browser can handle
// also goes straight through
assert.equal(
  sut('/ok', 'https://example.com/test'),
  '/ok',
);

assert.equal(
  sut('ok', 'https://example.com/test'),
  'ok',
);

// but relatve paths starting with the port are resolved
// against the provided basePath
assert.equal(
  sut(':3000', 'https://example.com/test'),
  'https://example.com:3000',
);

// even when the base path has a port and a path
assert.equal(
  sut(':3000', 'https://example.com/test:5000/test?q=15#bookmark'),
  'https://example.com:3000',
);

// along with attached path
assert.equal(
  sut(':3000/test?test=1#ok', 'https://example.com/test'),
  'https://example.com:3000/test?test=1#ok',
);

// even when the base path has a port and path
assert.equal(
  sut(':3000', 'https://example.com:5000/test?q=15#bookmark'),
  'https://example.com:3000',
);

// absolute paths go straight through even if there are ports
assert.equal(
  sut('https://test.com/ok:8888', 'https://example.com/test'),
  'https://test.com/ok:8888',
);

// different protocols
assert.equal(
  sut(':3000', 'http://example.com/test'),
  'http://example.com:3000',
);
