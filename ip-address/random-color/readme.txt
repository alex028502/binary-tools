this is the random number generator

it is only for creating a random two digit hex number to generate a random
colored favicon

However, I decided to do that in our format, and then convert the number back
to the standard format before passing it to imagemagick

So to test it, I have created a wrapper script that redefined Math.random and
then calls our random script.  Then I have tested its output using bash.

