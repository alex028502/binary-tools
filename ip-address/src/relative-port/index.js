// have have to pass in URL so that we can require it in
// the node tests.  it is called Lib

module.exports = function (Lib, relativePath, basePath) {
  if (relativePath.charAt(0) !== ':') {
    return relativePath;
  }

  const baseUrl = new Lib(basePath);
  // I think there will always be a protocol
  // in the current href
  const parts = baseUrl.origin.split(':');
  return `${parts[0]}:${parts[1]}${relativePath}`;
};
