#! /usr/bin/env bash

# for the most part, test coverage should stop us from forgetting
# to include unit tests in this list
# so no need to do anything fancy

node ./src/convert/unit.js
node ./src/relative-port/unit.js
node ./api/get-host-from-env/unit.js

./random-color/test.sh
