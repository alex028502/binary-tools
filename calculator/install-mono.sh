#! /usr/bin/env bash

set -e

cd $(dirname $0)

monodir=node_modules/@monorepo


rm -rf $monodir

if [[ "$1" == "clear" ]]
then
  exit 0
fi

deps="$PWD/../lib/fake-browser $PWD/../lib/format"

mkdir $monodir
cd $monodir
for dep in $deps
do
  ln -s $dep
done
