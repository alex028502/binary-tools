function cleanup {
  set -x
  # hyphen before each one to send message to group
  kill -SIGINT $(jobs -p | xargs -I {} echo -{})
}
trap cleanup EXIT
