#! /usr/bin/env bash

set -e

cd $(dirname $0)

find . -name '*.js' | grep -v node_modules | grep -v /web/ | grep -v /dist/
