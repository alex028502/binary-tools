/* eslint no-console: 0 */

// this environment variable already exists in this format
// in the ci set-up so let's just work with it

module.exports = function (env) {
  if (env.SERVER_ARGS) {
    return env.SERVER_ARGS.replace('--host', '').trim();
  }
  return '127.0.0.1';
};

// thanks https://stackoverflow.com/a/6090287
if (require.main === module) {
  console.log(module.exports(process.env));
}
