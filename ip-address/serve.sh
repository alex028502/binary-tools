#! /usr/bin/env bash

set -e

webport=5000
apiport=3000
echo starting the app in $1 on port $webport
echo connecting to server on $apiport
echo
echo the app in $1 should already be connected to a server
echo on port $apiport:
grep -R ":$apiport" $1
echo

node api $apiport &
apipid=$!

function cleanup {
  echo cleaning up api process $apipid
  kill $apipid || echo no api process to clean up
  echo done
}
trap cleanup EXIT

host=$(node api/get-host-from-env)
echo will server what is in $1 at $host:$webport
node_modules/.bin/http-server $1 -p $webport -a $host
