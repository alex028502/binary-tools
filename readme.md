# Binary tools project

This is a repository of binary / octal / hex tools.  Actually so far it's only
a [calculator](https://alex028502.gitlab.io/binary-tools/calculator/) and
a web page that tells you your
[ip address](https://alex028502.gitlab.io/binary-tools/ip-address/),
but it might have more tools one day

I mainly wanted to demo a few things about test coverage and javascript, but
might use this for other projects soon too.

This repository is really about programming ideas and tricks, but might is about
a cool idea for a number system too.

It also contains a cool way to keep internal npm packages in the same repo as
your main project, and not worry about publishing and version numbers, but still
have different dependencies for each package. (more on this below)

## Number system

The number system uses different symbols for different bases, so the base
doesn't have to be indicated anywhere.  You can also mix an match bases by
mixing and matching symbols.

### Binary

The numbers in these projects are basically binary, where high and low are
represented by `i` and `o` respectively. `i` is one. `io` is two.

### Octal

Octal numbers can be written with the digits `01234567`.  Really these are
used as abbreviations for three binary digits as follows:

0 ooo
1 ooi
2 oio
3 oii
4 ioo
5 ioi
6 iio
7 iii

So `71` means seven eights and one, but that is just an abbreviation for
`iiiooi`

### Hex

The only way to write Hex if by alternating binary and octal digits.  So sixteen
and eleven is `1i3`.  This looks confusing at first, but to a native octal
speaker, this is less confusing than `0x1B` is to a native decimal speaker.

The RGB code for red is `i1o0o0`.  The ip address for localhost is is
o1i1.o0o0.o0o0.o0o1, and LAN addresses usually start with i4o0.i2i0.
See your ip address in this hex format
[here](https://alex028502.gitlab.io/binary-tools/ip-address/),

### Other bases

The binary and octal digits can be combined in whichever way is convenient. This
is handy when you want to write something like three US gallons, two quarts, one
pint, eleven ounces, and four drams: `3.ioii3o4 gallons`

It looks confusing, but the natives can pick out which digits correspond to
which units.  Natives also know how to quickly double that by moving the binary
point one digit to the right, and then quickly regrouping the digits.  Something
like this:

```
3.ioii3o4 gallons
```

double it:
```
3i.oii3o4 gallons
```

break up the octal digits
```
oiii.oiioiioioo gallons
```

regroup
```

7.oiio6i0 gallons
```

Seven gallons, one quart, one pint, six ounces, and eight drams

This looks hard to an outsider, but the natives think it looks hard when we
half a number over and over again, and come up with stuff like _0.0625_, or
have to remember stuff like 512 just so we can double one a lot of times.

### Weights and measures

This alternative universe uses any existing binary measurements from our
universe:

distance: furlongs, miles
volume: US drams, ounces, pints, quarts, gallons
weight: drams, ounces, pounds
money: bits, dollars (pieces of eight)

For small measures of length, they use inches, and the binary fractions for
them that we are used to.  These inches don't fit nicely into furlongs. There
are 1i6i7o0 inches in a furlong.  But that's OK.  They aren't really used for
the same thing.  They sometimes call 1o0 inches a "cubit", but in general the
imaginary people in my universe don't create any names that don't already exist
in our universe, just like how we don't need any money
unit bigger than a dollar.

### Big numbers

When talking about big numbers, they sometimes call ioooooooooo "a thousand" and
ioooooooooooooooooooo "a million".

## Javascript Mono-repo system

One of the interesting things about this repository is that it has npm packages
that never need to be published.  NPM expects you to get your package from a
server.  When you use a local path as the repo path, it still caches it if you
don't change the version number. (at least last time I tried) If you just
include internal mono repo dependencies using a local path, then they can't
have their own external dependencies.  I don't want to change the version
numbers in internal packages because I want to be totally sure that whatever
code I am looking at in the mono repo is what is going to be used when I build.
From trial and error a few years ago, I came up with this solution (or hack)

- Put a bunch of npm packages into my mono repo
(some main projects, and some shared libraries)
- Install dependencies for all packages at once using a [script](./install.sh)
in the project root called
- When one of my main projects (calculator in this case) depends on an
internal library, do not try to get NPM to install it.  Just symlink the lib
project into the node_modules directory of the main project.  Node will have
no trouble finding the dependencies.  However, they dependencies of the libs
will not be _deduped_.  That's OK.

There are some nuances with `npm install` and `npm install --save` that take
a bit of getting used to, but once you have what you want in package-lock.json
and have all your symlinking set up in `postinstall`, `npm ci` works. Take a
look at `postinstall` in [package.json](./calculator/package.json) to see how
a main project symlinks all of the mono repo dependencies after NPM has finished
installing external dependencies.

## Subprojects

This mono-repo so far contains:

### The calculator project

[This](./calculator) is where you will find all the stuff about webpack,
testing, and 100% test coverage that I wanted to write down somewhere
Selenium tests here are written in python.

### The ip-address project

[This](./ip-address) is where you will find another web testing project that
includes an api, and a fake api for local testing. Selenium tests here are
written in javascript.

### Format library

[This](./lib/format) is a library that formats numbers into binary, octal, and
hex, and parses them as well.  I hope to use that in more projects in the
future.

It is an example of how even though sometimes 100% test coverage allows you to
safely refactor code, and make it super beautiful and readable, sometimes
readable tests that make totally sure your library does what it should allow
you to leave your implementation messy.

A deterministic library with no side effects is great for that.

## source control

I think I have quite good git habits:
- never `git merge`
- never `git pull`
- "atomic" commits

take a look at my project history with `gitk` to see!
