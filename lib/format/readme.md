# format library

This library formats and parses numbers written in the number system described
[here](..)

The implementations are all messy, but the tests are all straightforward.

Sometimes it is really straightforward to say what the answer should be in every
situtation, but telling a computer how to arrive at that answer is a messy
business.  Sometimes it's the other way round.

This doesn't use a test framework. It's all just simple synchronous functions
so just a file with assertions is good enough.

## mono repo

This is an example of an internal npm module using the mono repo system
described [here](..). It is symlinked into the parent projects' node_modules
directory, and then node finds it there. So when I make a change to this
library, I know that all the projects that use it are going to get that change
immediately, and that when I build all of these projects, they are always built
with the library as it is in that version of the repo.
