/* eslint-env node, mocha */
/* eslint no-console: 0 */

const assert = require('assert');
const promisify = require('util').promisify;

const webdriver = require('selenium-webdriver');
const Builder = webdriver.Builder;
const By = webdriver.By;

const axios = require('axios');

const convert = require('./src/convert');

const parse = require('@monorepo/format/parse');

const writeFile = promisify(require('fs').writeFile);

const sleep = promisify(function(interval, callback) {
  // setTimeout has the callback at the start instead of the end
  setTimeout(callback, interval);
});

describe('doesNotContain', function () {
  it('knows when it does not contain', function () {
    // the opposite case will be asserted throughout
    // the tests so does not need to be tested
    assert.ok(doesNotContain('xyz', 'a'));
    // actually
    assert.equal(doesNotContain('xyz', 'a'), 'xyz does not contain a');
  });
});

describe('hostFromEnv', function() {
  it('should work with a specified host', function() {
    const expectedHost = 'test';
    const env = {
      SERVER_ADDRESS: expectedHost,
    };
    assert.equal(hostFromEnv(env), expectedHost);
  });

  it('should work without a specified host', function() {
    assert.equal(hostFromEnv({}), 'localhost');
  });
});

describe('the log helper', function() {
  it('will not fail if there is no browser', function() {
    return showLogsFrom(null);
  });
});

describe('binary ip address app', function() {
  let driver;
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').setLoggingPrefs({
      browser: 'ALL',
    }).build();
  });

  it('works when the server works', async function () {
    const ipAddress = '111.111.111.111';
    await setResponse(ipAddress);
    // let's calculate this ourselves
    assert.equal(111, 64 + 32 + 8 + 4 + 2 + 1);
    assert.equal(111, 16 * (4 + 2) + 8 + 4 + 2 + 1);
    // binary oiioiiii
    // hex o6i7
    const expectedResult = 'o6i7 o6i7 o6i7 o6i7';
    await expectPageToContain(driver, expectedResult);
  });

  it('works when the server works (random)', async function () {
    // let's make a random one
    // even thought that is not always considered the best practice
    // I am pretty confident
    const numbers = [];
    while (numbers.length < 4) {
      numbers.push(Math.floor(Math.random() * 255));
    }

    const ipAddress = numbers.join('.');
    await setResponse(ipAddress);
    await expectPageToContain(driver, convert(ipAddress));
  });

  it('works when the server works (random hex)', async function () {
    // the above is too easy because it uses the same convert
    // method as the implementation
    // let's do it backwards by randomly generating an ip address
    // in our new format, and then working out the corresponding
    // decimal coded format, and then make sure it all matches up
    const numbers = [];
    while (numbers.length < 4) {
      numbers.push(randomSet());
    }
    const expectedResponse = numbers.join(' ');
    const ipAddress = numbers.map(parse).join('.');
    await setResponse(ipAddress);
    await expectPageToContain(driver, expectedResponse);
  });

  it('gives a nice message when there is a 500', async function () {
    await setResponse('500');
    await expectPageToContain(driver, 'ERROR');
  });

  it('gives a nice message when there is a 400', async function () {
    await setResponse('400');
    await expectPageToContain(driver, 'ERROR');
  });

  afterEach(async function() {
    const coverage = await driver.executeScript(
      'return JSON.stringify(window.__coverage__);',
    );
    writeFile(
      `${__dirname}/.coverage/${Math.floor(Date.now() / 1000)}.json`,
      coverage,
    );
  });

  afterEach(function() {
    return showLogsFrom(driver);
  });

  afterEach(function() {
    return sleep(500).then(function() {
      return driver.quit();
    });
  });
});

async function showLogsFrom(driver) {
  if (driver) {
    const logs = await driver.manage().logs().get('browser');
    for (const entry of logs) {
      console.log(entry.level.name_, entry.message);
    }
  }
}

function hostFromEnv(env) {
  if (env.SERVER_ADDRESS) {
    return env.SERVER_ADDRESS;
  }
  return 'localhost';
}

function setResponse(response) {
  const host = hostFromEnv(process.env);
  return axios.post('http://' + host + ':3000?response=' + response);
}

async function expectPageToContain(driver, string) {
  const host = hostFromEnv(process.env);
  await driver.get('http://' + host + ':5000');

  await sleep(500);
  const bodyTag = await driver.findElement(By.tagName('body'));

  const pageContent = await bodyTag.getAttribute('innerHTML');

  assert.equal(doesNotContain(pageContent, string), false);
}

function doesNotContain(haystack, needle) {
  // chai has something for expect(haytack).to.include(needle)
  // but we are using the built-in assert lib
  // so we will just assert that this function returns false
  if (haystack.includes(needle)) {
    // false is good - sorry
    return false;
  }
  return `${haystack} does not contain ${needle}`;
}

function randomSet() {
  return randomHex() + randomHex();
}

function randomHex() {
  return randomBin() + randomOct();
}

function randomBin() {
  return randomFrom('oi');
}

function randomOct() {
  return randomFrom('01234567');
}

function randomFrom(l) {
  const idx = Math.floor(Math.random() * l.length);
  return l[idx];
}
