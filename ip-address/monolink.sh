#! /usr/bin/env bash

set -e

cd node_modules

rm -rf @monorepo

if [[ "$1" == "clear" ]]
then
  exit 0
fi

mkdir @monorepo

cd @monorepo

ln -s ../../../lib/format
