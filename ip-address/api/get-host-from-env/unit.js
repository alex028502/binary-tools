const assert = require('assert');

const sut = function (serverArgs) {
  return require('.')({
    SERVER_ARGS: serverArgs,
  });
};

assert.equal(sut('--host 0.0.0.0'), '0.0.0.0');
assert.equal(sut(''), '127.0.0.1');
assert.equal(sut(), '127.0.0.1');
