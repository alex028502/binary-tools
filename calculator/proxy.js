// test server for testing service worker caching
// can be turned on and off

const express = require('express');

const httpProxy = require('http-proxy');
const enableDestroy = require('server-destroy');

const target = process.argv[2];
const host = process.argv[3];
const port = 8000;
const controlPort = 8001;

let proxyServer = null;
const proxyApp = httpProxy.createProxyServer({
  target: target,
});

const controlApp = express();

controlApp.get('/', function(req, res) {
  const status = proxyServer ? 'ON' : 'OFF';
  const form = '<form method="post"><input type="submit" value="i/o"></form>';
  res.send(`currently ${status}<br />${form}`);
});

controlApp.post('/', async function(req, res) {
  if (proxyServer) {
    proxyServer.destroy();
    proxyServer = null;
  } else {
    proxyServer = await new Promise(function(resolve) {
      // we have to put the options in an object because listen only passes
      // two arguments on to the function it wraps, so we can't put
      // port, host, callback
      const options = {
        port: port,
        host: host,
      };
      const server = proxyApp.listen(options, function(x) {
        enableDestroy(server);
        resolve(server);
      });
    });
  }
  res.redirect('/');
});

controlApp.listen(controlPort, host, function() {
  console.log('proxied address', target);
  console.log('see proxied site on port', port);
  console.log('turn proxy on and off on port', controlPort);
});
