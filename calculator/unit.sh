#! /usr/bin/env bash

set -e

cd $(dirname $0)

for filename in $(find src -name unit.js)
do
  node $filename
done
